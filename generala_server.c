#include <time.h>
#include "generala.h"

/*
	 0 		se tacho.
	-1 		nunca se jugo.
	otro 	valor jugado.
	
	Hay [0, MAX_JUGADORES) id's
		
	id_jugador -1 implica no se registro un jugador para esa id.
*/
struct tabla tablas;

int id = 0; // Para asignar. 
int estado = WAIT_JUGADORES;
int terminados = 0;	//Para mantener la cantidad que terminaron de jugar

struct turno jugada_actual;

struct dados getDados(int cuantos) {
	struct dados  result;

	srand(time(NULL));
	int i;
	for (i = 0; i < cuantos && i < 5; i++) {
		result.numeros[i]=(rand()%6)+1;
	}	
	result.cant_dados = i;
	
	return result;
}

int * anotarse_1_svc(void *argp, struct svc_req *rqstp) {
	static int  result;
	result = -1;
	
	// primera vez que alguien se anota.
	// se inicializan tablas.
	if (id == 0) {  
		tablas.cant_jugadores = 0;
		for (int i = 0; i < MAX_JUGADORES; i++) {
			for (int j = 0; j < CANT_CATEGORIAS; j++) {
				tablas.tabla_completa[i].categoria[j] = -1; 
			}
			tablas.tabla_completa[i].id_jugador = -1;
		}
		
	}
	
	// Estos valores implican que no hay nadie jugando. No se puede inicializar fuera de una funcion. 
	jugada_actual.id_jugador = -1;
	jugada_actual.jugada.cant_dados = -1;
	
	if (estado != JUGANDO && tablas.cant_jugadores < MAX_JUGADORES) {
		
		tablas.tabla_completa[id].id_jugador = id;		
		result = id; // Id otorgada.
		
		tablas.cant_jugadores++;
		id++;						
				
		if (tablas.cant_jugadores > 1) {
			estado = WAIT_INICIO;			
		}
		printf("[anotarse] nuevo jugador con id %d\n", result);
	}

	return &result;
}

int * iniciar_1_svc(void *argp, struct svc_req *rqstp) {
	static int  result;
	result = -1;
	
	if (estado == WAIT_INICIO) {
		estado = JUGANDO;
		result = 1;
		
		jugada_actual.id_jugador = 0;
		jugada_actual.jugada.cant_dados = 0;						
	}

	return &result;
}

int * estado_juego_1_svc(void *argp, struct svc_req *rqstp) {
	static int  result;
	result = estado;
	return &result;
}

struct turno * quien_juega_1_svc(int *argp, struct svc_req *rqstp) {
	static struct turno  result;
	result = jugada_actual;

	return &result;
}

int * fin_turno_1_svc(int *argp, struct svc_req *rqstp) {
	static int  result;
	result = 1;
	
	jugada_actual.id_jugador = (jugada_actual.id_jugador + 1) % tablas.cant_jugadores;
	jugada_actual.jugada.cant_dados = 0;
	
	return &result;
}

struct tabla * resultados_1_svc(void *argp, struct svc_req *rqstp)
{
	static tabla  result;

	// Se copia tabla
	result.cant_jugadores = tablas.cant_jugadores;
	for (int i = 0; i < tablas.cant_jugadores; i++) {
		for (int j = 0; j < CANT_CATEGORIAS; j++) {
			result.tabla_completa[i].categoria[j] = tablas.tabla_completa[i].categoria[j]; 
		}
		result.tabla_completa[i].id_jugador = tablas.tabla_completa[i].id_jugador;
	}

	return &result;
}

/**
 * Se le da un struct dados con -1 en los lugares donde se necesita un valor nuevo.
 * */
struct dados * tirar_dados_1_svc(struct dados *argp, struct svc_req *rqstp) {
	static struct dados result;

	srand(time(NULL));
	for (int i = 0; i < 5; i++) {
		if (argp->numeros[i] == -1 ) {
			result.numeros[i] = (rand()%6) + 1;
		} else {
			result.numeros[i] = argp->numeros[i];
		}		
	}	
	result.cant_dados = 5;
	jugada_actual.jugada = result;

	return &result;
}

int *
anotar_1_svc(struct anotacion *argp, struct svc_req *rqstp)
{
	static int  result;
	
	int id_jugador = jugada_actual.id_jugador;

	printf("\nJugador %d anota:\n", id_jugador);
	printf("  Categoria: %i\n", argp->categoria);				
	printf("  Valor: %i\n", argp->valor);	
	
	tablas.tabla_completa[id_jugador].categoria[argp->categoria] = argp->valor;

	printf("Anote %i al %i\n", tablas.tabla_completa[id_jugador].categoria[argp->categoria] , argp->categoria);
	result = 1;
	return &result;
}

int * termine_1_svc(int *argp, struct svc_req *rqstp) {
	static int  result;

	if(estado!=TERMINADO)
		terminados++;

	if(terminados==tablas.cant_jugadores)
		estado=TERMINADO;

	result = 0;
	
	return &result;
}
