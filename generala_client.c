#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include "generala.h"
#include "reglas_generala.c"

CLIENT *clnt;
int  *result_1;
char *anotarse_1_arg;
int  *result_2;
char *iniciar_1_arg;
int  *result_3;
char *estado_juego_1_arg;
struct turno  *result_4;
int  quien_juega_1_arg;
int  *result_5;
int  fin_turno_1_arg;
struct tabla  *result_6;
char *resultados_1_arg;
struct dados  *result_7;
struct dados *tirar_dados_1_arg;
int  *result_8;
struct anotacion anotar_1_arg;
int  *result_9;
int  termine_1_arg;

char *host;

int mi_id;
int estado_server;

struct tabla_jugador mis_puntos;

void printResultados() {
	result_6 = resultados_1((void*)&resultados_1_arg, clnt);
	if (result_6 == (struct tabla *) NULL) {
		clnt_perror (clnt, "[resultados] call failed");
		exit(1);
	}
	
	printf("┌───────────────────┐\n");
	printf("│ Categoría │ Valor │\n");
		
	int valor;

	//Variables que solo sirven cuando es el ultimo print, habria que hacerlo de otra manera
	int resultados_finales[result_6->cant_jugadores];
	int jugadores_finales[result_6->cant_jugadores]; 	//Esto es para mantener correlacion entre resultado-jugador
	for (int m = 0; m < result_6->cant_jugadores; m++)
	{
		resultados_finales[m]=0;
		jugadores_finales[m]=m;
	}
	
	for (int i = 0; i < result_6->cant_jugadores; i++){
		printf("┌───────────────────┐\n");
		printf("│    Jugador %d      │\n", result_6->tabla_completa[i].id_jugador);
		printf("└───────────────────┘\n");
		
		for (int j = 0; j < CANT_CATEGORIAS; j++) {
			valor = result_6->tabla_completa[i].categoria[j];
			//printf("Categoria %i Valor %i \n", j, valor );
			if(estado_server==TERMINADO)
				resultados_finales[i]+=valor;
			
			if (valor >= 0) {
				switch (j){
				case UNO:
					printf("│ 1         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case DOS:             
					printf("│ 2         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case TRES:            
					printf("│ 3         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case CUATRO:          
					printf("│ 4         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case CINCO:           
					printf("│ 5         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case SEIS:            
					printf("│ 6         │");
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case ESCALERA: 
					printf("│ Escalera  │");       
					if(valor)
						printf(" %i\t\t\n", valor );
					else
						printf(" %s\t\t\n","X" );
				break;                
				case FULL:            
					printf("│ Full      │");
					if(valor)
						printf(" %i\t\t\n", valor);
					else
						printf(" %s\t\t\n","X" );
				break;                
				case POKER: 
					printf("│ Poker     │");
					if(valor)
						printf(" %i\t\t\n", valor);
					else
						printf(" %s\t\t\n","X" );
				break;                
				case GENERALA:
					printf("│ Generala  │");
					if(valor)
						printf(" %i\t\t\n", valor);
					else
						printf(" %s\t\t\n","X" );

				break;                
				case GENERALADOBLE:   
					printf("│ Doble     │");
					if(valor)
						printf("%i\t\t\n", valor);
					else
						printf(" %s\t\t\n","X" );

				break;
				}			
			}	
		}
	} 
	printf("└───────────────────┘\n");	
	if(estado_server==TERMINADO){
		//Ordeno los resultados de mayor a menor
		//int max = resultados_finales[0];
		for (int l = 0; l < result_6->cant_jugadores ; l++)
		{
			//if(max<resultados_finales[l])
			for (int r = 0; r < result_6->cant_jugadores; r++)
			{
				if(resultados_finales[r] < resultados_finales[l])
				{
					int tmp_1 = resultados_finales[l];
					int tmp_2 = jugadores_finales[l];
					resultados_finales[l]=resultados_finales[r];
					resultados_finales[r]=tmp_1;
					jugadores_finales[l]=jugadores_finales[r];
					jugadores_finales[r]=tmp_2;

				}
			}
		}
		printf("Los resultados de la partida son:\n");
		for (int i = 0; i < result_6->cant_jugadores; i++)
		{
			printf("%i°) Jugador %i : %i puntos\n", i+1,jugadores_finales[i], resultados_finales[i] );
		}

		int resultado_ganador = resultados_finales[0];
		int cant_ganadores = 1;
		int termine_busqueda=0;
		for (int j = 1; (j < result_6->cant_jugadores) && !termine; j++)
		{
			if (resultado_ganador==resultados_finales[j])	
				cant_ganadores++;
			else
				termine_busqueda=1;
		}
		if(cant_ganadores==1)
			printf("Felicitaciones Jugador %i, Ganaste!\n", jugadores_finales[0] );
		else
			printf("Hubo empate entre %i jugadores, Jueguen otra para desempatar! \n", cant_ganadores );
	}	
}

void anotando(struct dados *jugada, int n_tiros) {
	int loopear = 1;
	int tachar, opcion;	
	
	while (loopear){
		printf("Categorias: \n");		
		printf("	 1- Unos \n");
		printf("	 2- Dos \n");
		printf("	 3- Tres \n");
		printf("	 4- Cuatros \n");
		printf("	 5- Cincos \n");
		printf("	 6- Seis \n");
		printf("	 7- Escalera \n");
		printf("	 8- Full \n");
		printf("	 9- Poker \n");
		printf("	 10- Generala \n");
		printf("	 11- Generala Doble \n");
		scanf("%i",&opcion);
		while(mis_puntos.categoria[opcion-1]!=-1){
			printf("Elija una opcion que todavia no jugo!\n");
			printf("Elija que categoría desea anotar/tachar: \n");	
			scanf("%i",&opcion);
		}
		printf("\nDesea...\n0- anotar\n1- tachar\n");
		scanf("%i",&tachar);
		
		opcion--;
		//Hay que validar que la opcion entre en el rango aceptado, que tachar no tome un valor absurdo, y que la categoria a anotar/tachar no se haya jugado antes
		if (opcion>=UNO && opcion<=GENERALADOBLE && (tachar==1 || tachar==0)) {
			loopear = 0;			
		} else {
            printf("\nIngrese opciones validas\n");            
        }
	}
	
	anotar_1_arg.tachar = tachar;
	anotar_1_arg.categoria = opcion;
	// equivale a tachar o que el jugador haya dicho tener algo y no lo tenia
	anotar_1_arg.valor = 0;
	mis_puntos.categoria[opcion]=0;		//O lo tacho o lo sobreescribe despues
	
	if (!tachar) 
	{		
		if (opcion >= UNO && opcion <= SEIS) {
			anotar_1_arg.valor = suma_numero(jugada, opcion+1);

			//printf("quiero anotarle %i al %i\n", anotar_1_arg.valor, opcion+1 );
		} else {
			switch (opcion) {
				case ESCALERA:
					if (hay_escalera(jugada)) {
						anotar_1_arg.valor = VALOR_ESCALERA;
					}					
				break;
				case FULL:
					if (hay_full(jugada)) {
						anotar_1_arg.valor = VALOR_FULL;
					}					
				break;
				case POKER:
					if (hay_poker(jugada)) {
						anotar_1_arg.valor = VALOR_POKER;
					}					
				break;
				case GENERALA:
					if (hay_generala(jugada)) {
						anotar_1_arg.valor = VALOR_GENERALA;
					}					
				break;
			}
			if (n_tiros == 1 && anotar_1_arg.valor>0) {
				anotar_1_arg.valor += VALOR_ADICIONAL_SERVIDA;
			}
		}
		mis_puntos.categoria[opcion]=anotar_1_arg.valor;
	}
	
	printf("\nAnotacion \n");
	printf("Categoria: %i\n", opcion);				
	printf("Valor: %i\n", anotar_1_arg.valor);
	
	result_8 = anotar_1(&anotar_1_arg, clnt);
	if (result_8 == (int *) NULL) {
		clnt_perror (clnt, "[anotar] call failed");
		exit(1);
	} 
}

void printDados(int numeros[], int cuantos)
{
	char* opciones[7];
	opciones[0] = "╔═══════╗";
	opciones[1] = "║       ║";
	opciones[2] = "║●      ║";
	opciones[3] = "║      ●║";
	opciones[4] = "║   ●   ║";
	opciones[5] = "║●     ●║";
	opciones[6] = "╚═══════╝";

	char* dadosToPrint[6][3];
	// 1
	dadosToPrint[0][0] = opciones[1];
	dadosToPrint[0][1] = opciones[4];
	dadosToPrint[0][2] = opciones[1];
	// 2
	dadosToPrint[1][0] = opciones[2];
	dadosToPrint[1][1] = opciones[1];
	dadosToPrint[1][2] = opciones[3];
	// 3
	dadosToPrint[2][0] = opciones[2];
	dadosToPrint[2][1] = opciones[4];
	dadosToPrint[2][2] = opciones[3];
	// 4
	dadosToPrint[3][0] = opciones[5];
	dadosToPrint[3][1] = opciones[1];
	dadosToPrint[3][2] = opciones[5];
	// 5
	dadosToPrint[4][0] = opciones[5];
	dadosToPrint[4][1] = opciones[4];
	dadosToPrint[4][2] = opciones[5];
	// 6
	dadosToPrint[5][0] = opciones[5];
	dadosToPrint[5][1] = opciones[5];
	dadosToPrint[5][2] = opciones[5];

	// Parte superior	
	for (int i = 0; i < cuantos; i++) {
		printf("%s ", opciones[0]);
	}
	printf("\n");

	// 3 filas que varian 
	for(int i=0; i<3; i++) {
		// cuantos numeros
		for(int j=0; j< cuantos;j++) {
			printf("%s ",dadosToPrint[numeros[j]-1][i]);
		}
		printf("\n");
	}
	
	// Parte inferior	
	for (int i = 0; i < cuantos; i++) {
		printf("%s ", opciones[6]);
	}
	printf("\n");
}

/**
Pone en jugada los dados finales del jugador.
Retorna el numero de tiros.
*/
int jugar(struct dados *jugada) {
		
	jugada->cant_dados = 5;
	for (int i = 0; i < jugada->cant_dados; i++) {
		jugada->numeros[i] = -1;
	}
	
	int tiro = 1;
	tirar_dados_1_arg = jugada;
	int n_cambiar;
	do {
		result_7 = tirar_dados_1(tirar_dados_1_arg, clnt);
		if (result_7 == (struct dados *) NULL) {
			clnt_perror (clnt, "[tirar_dados] call failed");
			exit(1);
		}
		
		// Copiar               
        for (int i = 0; i < jugada->cant_dados; i++) {
			jugada->numeros[i] = result_7->numeros[i];
		}                 
        	
		printf("Nuevos dados, tiro %d\n", tiro);
		printDados(jugada->numeros, jugada->cant_dados);
		
		if (tiro < 3) {
			printf("Cuantos dados desea re tirar(0-5):\n");
			scanf("%d", &n_cambiar);
			
			int i = 0;
			int nro_dado;
			while (i < n_cambiar) {
				printf("Indique dado %d a re tirar (1-5 izq a der):\n", i+1);
				scanf("%d", &nro_dado);
				jugada->numeros[nro_dado -1] = -1;			
				i++;
			}	
		}		
		
		tiro++;			
	} while (n_cambiar != 0 && tiro <= 3);
	
	return tiro - 1;	
}

void iniciar_esperar() {
	int loopear=1;
	int opcion;
	while (loopear) {
		printf("0- Esperar\n1- Iniciar el juego\n");
		scanf("%i",&opcion);
		
		if (opcion!=0 && opcion!=1) {
			printf("Opcion invalida\n");			
		} else {
			if (opcion!=1) {
				printf("Esperando que alguien inicie el juego...\n");
				loopear = 0;
			} else {				
				result_3 = estado_juego_1((void*)&estado_juego_1_arg, clnt);
				if (result_3 == (int *) NULL) {
					clnt_perror (clnt, "[estado] call failed");
					exit(1);
				}
				estado_server = *result_3;
				
				if (*result_3 == JUGANDO) {
					printf("El juego ya inicio\n");
					loopear = 0;
				} else {
					if (*result_3 == WAIT_JUGADORES) {
						printf("Faltan jugadores. Espere e intente de nuevo\n");
					} else {
						result_2 = iniciar_1((void*)&iniciar_1_arg, clnt);
						if (result_2 == (int *) NULL) {
							clnt_perror (clnt, "[iniciar] call failed");
							exit (1);
						}
				
						if (*result_2 == 1) {
							printf("Inicia el juego...\n");
							loopear = 0;
						}					
					}
				}								
			}			
		}
	}
}

void creacion_cliente() {
	clnt = clnt_create (host, JUEGO_GENERALA, GENERALAVERS, "udp");
	if (clnt == NULL) {
		printf("[clnt_create] error");
		clnt_pcreateerror (host);
		exit (1);
	}			
	
	result_1 = anotarse_1((void*)&anotarse_1_arg, clnt);
	if (result_1 == (int *) NULL) {
		clnt_perror (clnt, "[anotarse] call failed");
		exit (1);
	} else {
		if (*result_1 == -1) {
			printf("El juego ya empezo o se llego al maximo de jugadores\n");
			exit(0);
		} else {
			mi_id = *result_1;
			printf("Soy el jugador con id %d\n", mi_id);
		}		
	}
}

/**
	Chequea si debe actualizar la jugada mostrada de otro jugador. 
	1: debe actualizar.
	0: no debe actualizar.
**/
int actualizarJugada(struct turno *previo, struct turno *actual) {		

	int cambio_jugador = (previo->id_jugador != actual->id_jugador);
	int cambiaron_dados = (previo->jugada.cant_dados != actual->jugada.cant_dados);
	if  (actual->jugada.cant_dados > 0 && 
		( cambio_jugador || cambiaron_dados)) {
			
		return 1;
	}
	
	for(int i = 0; i < previo->jugada.cant_dados; i++) {
		if (previo->jugada.numeros[i] != actual->jugada.numeros[i]) {
				
			return 1;
		}
	}
	
	return 0;	
}

int main (int argc, char *argv[]) {	
	int turnos_jugados = 0;

	host= malloc(16);

	if (argc < 2) {
		printf ("usage: %s server_host\n", argv[0]);
		exit (1);
	}

	strcpy(host,argv[1]);
	
	creacion_cliente();

	mis_puntos.id_jugador=mi_id;
	//Inicializo el arreglo donde guardo mis puntos actuales
	for (int i = 0; i < CANT_CATEGORIAS; i++)
	{
		mis_puntos.categoria[i]=-1;
	}
	
	iniciar_esperar();	
	
	struct turno *previo = malloc(sizeof(struct turno));
	// Valores nadie jugando
	previo->id_jugador = -1;
	previo->jugada.cant_dados = -1;
	
	struct turno *actual = malloc(sizeof(struct turno));	
	
	while (turnos_jugados < CANT_CATEGORIAS) {
		usleep(200);		
		result_4 = quien_juega_1(&quien_juega_1_arg, clnt);
		if (result_4 == (struct turno *) NULL) {
			clnt_perror (clnt, "[quien_juega] call failed");
			exit(1);
		}
		
		// Copiar
		actual->id_jugador = result_4->id_jugador;
		actual->jugada = result_4->jugada; 			
		
		if (actual->id_jugador == mi_id) {			
			printf("═══════════════════════════ TURNO %d ═══════════════════════════\n", turnos_jugados+1);
										
			struct dados *ptr_dados = malloc(sizeof(struct dados));
			
			// Devuelve dados finales en ptr_dados
			int n_tiros = jugar(ptr_dados);
			
			anotando(ptr_dados, n_tiros);
			
			free(ptr_dados);
			
			usleep(1000); // Pasa que pide resultados que aun no fueron anotados 
			printResultados();
			
			printf("═══════════════════════════ FIN TURNO %d ═══════════════════════════\n", turnos_jugados);
			
			turnos_jugados++;
			
			usleep(500); // Otros jugadores leen mi ultima jugada.
			result_5 = fin_turno_1(&fin_turno_1_arg, clnt);
			if (result_5 == (int *) NULL) {
				clnt_perror (clnt, "[fin_turno] call failed");
				exit(1);
			}
		} else if (actualizarJugada(previo, actual)) {
			// Copiar 
			previo->id_jugador = actual->id_jugador;
			previo->jugada = actual->jugada;
			
			printf("Dados tirados por jugador %d\n", actual->id_jugador);
			printDados(actual->jugada.numeros, actual->jugada.cant_dados);
		}			
	}


	result_9 = termine_1(&termine_1_arg, clnt);
	if (result_9 == (int *) NULL) {
		clnt_perror (clnt, "call failed");
	}

	while(estado_server !=TERMINADO){
		usleep(200);
		result_3 = estado_juego_1((void*)&estado_juego_1_arg, clnt);
		if (result_3 == (int *) NULL) {
			clnt_perror (clnt, "[estado] call failed");
			exit(1);
		}
		estado_server = *result_3;
		
		result_4 = quien_juega_1(&quien_juega_1_arg, clnt);
		if (result_4 == (struct turno *) NULL) {
			clnt_perror (clnt, "[quien_juega] call failed");
			exit(1);
		}
		
		actual->id_jugador = result_4->id_jugador;
		actual->jugada = result_4->jugada;
		
		if( actualizarJugada(previo, actual) )
		{
			previo->id_jugador = actual->id_jugador;
			previo->jugada = actual->jugada;
			
			printf("Dados tirados por jugador %d\n", actual->id_jugador);
			printDados(actual->jugada.numeros, actual->jugada.cant_dados); 
		}
	}
	printResultados();

	usleep(3000);
	
	free(previo);
	free(actual);

	clnt_destroy (clnt);
	
	exit (0);
}
