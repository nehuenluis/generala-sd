#include "generala.h"
#include <stdio.h>
#include <stdlib.h>

int hay_full(struct dados *dados_tirados){
/*	
 *  Recibe los dados obtenidos
 *	Verifica si hay full, es decir tres numeros iguales y un par
 * 	Devuelve 1 si hay y 0 en caso contrario
 *
 */
	struct dados * dados_tmp=dados_tirados;

	int cantidad_de_apariciones[6];

	//Inicializar el arreglo contador en 0
	for (int i = 0; i < 6; i++)
	{
		cantidad_de_apariciones[i]=0;
	}

	int v_hayfull=0;
	int hay2=0;
	int hay3=0;

	
	//Recorrer el arreglo recibido guardando la cantidad de veces que aparece cada valor
	for (int j = 0; j < dados_tmp->cant_dados; j++)
	{
		//incremento en 1 la cantidad de veces que aparecio ese valor
		cantidad_de_apariciones[ (dados_tmp->numeros[j]) - 1 ]++;
		
	}
	for (int i = 0; (i < 6) && !v_hayfull; i++)
	{
		if(cantidad_de_apariciones[i]==2)
		{	//veo si ya encontre 3
			if (hay3)	
				v_hayfull=1;	//hay 3 y 2
			else
				hay2=1;		//indico que hay 2
		}
		else if (cantidad_de_apariciones[i]==3)
		{	//veo si ya encontre 2
			if(hay2)
				v_hayfull=1;	//hay 2 y 3
			else
				hay3=1;	//indico que hay 3
		}	
	}
	return v_hayfull;
}

int hay_generala(struct dados *dados_tirados){
/*	
 *  Recibe los dados obtenidos
 *	Verifica si hay generala, es decir todos los numeros iguales
 * 	Devuelve 1 si hay y 0 en caso contrario
 *
 */
	struct dados * dados_tmp=dados_tirados;

	int cantidad_de_apariciones[6];

	//Inicializar el arreglo contador en 0
	for (int i = 0; i < 6; i++)
	{
		cantidad_de_apariciones[i]=0;
	}

	int v_haygenerala=0;
	
	//Recorrer el arreglo recibido guardando la cantidad de veces que aparece cada valor
	for (int j = 0; ( (j < dados_tmp->cant_dados) &&  (!v_haygenerala) ); j++)
	{
		//incremento en 1 la cantidad de veces que aparecio ese valor
		if( (++cantidad_de_apariciones[ (dados_tmp->numeros[j]) - 1 ]) == 5)
			v_haygenerala=1;	//Si llego a 4 apariciones de uno hay poker
	}

	return v_haygenerala;
}

int hay_poker(struct dados *dados_tirados){
/*	
 *  Recibe los dados obtenidos
 *	Verifica si hay poker, es decir cuatro numeros iguales
 * 	Devuelve 1 si hay y 0 en caso contrario
 *
 */
	struct dados * dados_tmp=dados_tirados;

	int cantidad_de_apariciones[6];

	//Inicializar el arreglo contador en 0
	for (int i = 0; i < 6; i++)
	{
		cantidad_de_apariciones[i]=0;
	}

	int v_haypoker=0;

	//Recorrer el arreglo recibido guardando la cantidad de veces que aparece cada valor
	for (int j = 0; ( (j < (dados_tmp->cant_dados)) &&  (!v_haypoker) ); j++)
	{
		//incremento en 1 la cantidad de veces que aparecio ese valor
		if( (++cantidad_de_apariciones[ (dados_tmp->numeros[j]) - 1 ]) == 4)
			v_haypoker=1;	//Si llego a 4 apariciones de uno hay poker
	}
	return v_haypoker;
}

int hay_escalera(struct dados *dados_tirados){
/*	
 *  Recibe los dados obtenidos
 *	Verifica si hay escalera, ya sea 1-2-3-4-5 o 2-3-4-5-6
 * 	Devuelve 1 si hay y 0 en caso contrario
 *
 */
	struct dados * dados_tmp=dados_tirados;

	int arreglo_auxiliar[dados_tmp->cant_dados];	//creo un arreglo auxiliar
	for (int n = 0; n < dados_tmp->cant_dados; n++)	//copio los dados recibidos al arreglo auxiliar
	{
		arreglo_auxiliar[n] = dados_tmp->numeros[n];
	}

	int valor_temporal=0;

	//Ordeno el arreglo de forma ascendente asi es mas facil ver si hay escalera o no
	for (int i = 0; i < dados_tmp->cant_dados; i++)
	{
		for (int j = 0; j < dados_tmp->cant_dados; j++)
		{
			if (arreglo_auxiliar[j]>arreglo_auxiliar[i])
			{
				valor_temporal = arreglo_auxiliar[i];
				arreglo_auxiliar[i] = arreglo_auxiliar[j];
				arreglo_auxiliar[j] = valor_temporal;
			}	
		}
	}

	if (arreglo_auxiliar[0] == 1 || arreglo_auxiliar[0] == 2) //Verifico que los numeros iniciales sean 1 o 2 para no evaluar el arreglo sin sentido
	{
		int valor_actual = arreglo_auxiliar[0];
		valor_actual++;
		int v_hayescalera=1;
		for (int k = 1; ( (k < (dados_tmp->cant_dados) ) && v_hayescalera ) ; k++)	
		{	//Voy sumando 1 en cada iteracion mientras el proximo valor tambien sea un incremento de 1 del anterior hasta llegar al final
			if(arreglo_auxiliar[k]==valor_actual)
				//Todavia sigo en carrera
				valor_actual++;
			else
				//Valor distinto al que deberia ser, no hay escalera
				v_hayescalera=0;
		}

		return v_hayescalera;
	}
	else
		return 0;
}

int suma_numero(struct dados *dados_tirados, int numero) {
/*
 *	Recibe los dados obtenidos y un numero que representa una cara de
 *  de los dados.
 * 	Devuelve la suma total en los dados del numero dado.
 *
 */
	int suma_calculada = 0;
	for (int i = 0; i < dados_tirados->cant_dados; i++) {
		if (dados_tirados->numeros[i] == numero) {
			suma_calculada += numero;
		}
	}
	
	return suma_calculada;
}
/*

int main(int argc, char const *argv[])
{
	struct dados *mis_dados = malloc(sizeof(struct dados));

	mis_dados->cant_dados=5;
	printf("ingrese los numeros obtenidos en los dados\n");
	int numero=0;
	int suma=0;
	
	for (int i = 0; i < 5; i++)
	{
		printf("ingrese el dado %i: ", i+1 );
		int temp = 0;
		scanf("%i",&temp);
		mis_dados->numeros[i] = temp;
	}
	printf("Los dados ingresados son: ");
	for (int j = 0; j < 5; j++)
	{
		printf("%i, ", mis_dados->numeros[j] );
	}

	printf("\n");
	int temp=0;
	printf("Ingresa que numero queres verificar: ");
	scanf("%i",&temp);
	numero=temp;

	printf("Ingresa la suma que deberia dar:\n");
	scanf("%i",&temp);
	suma=temp;
	
	printf("%s\n", "En el conjunto de dados ingresados:" );

	printf("%s hay GENERALA.\n", hay_generala(mis_dados) ? "Si" : "No" );
	printf("%s hay POKER.\n", hay_poker(mis_dados) ? "Si" : "No" );
	printf("%s hay FULL.\n", hay_full(mis_dados) ? "Si" : "No" );
	printf("%s hay ESCALERA.\n", hay_escalera(mis_dados) ? "Si" : "No" );

	printf("%s se verifica la suma.\n", hay_numero(mis_dados,numero,suma) ? "Si" : "No" );

	printf("%s\n", "Terminando la ejecucion." );

	

	return 0;
}*/
