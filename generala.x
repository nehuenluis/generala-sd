const MAX_JUGADORES = 5;
const CANT_CATEGORIAS = 11;

const WAIT_JUGADORES = 0;
const WAIT_INICIO = 1;
const JUGANDO = 2;
const TERMINADO = 3;

const UNO = 0;
const DOS = 1;
const TRES = 2;
const CUATRO = 3;
const CINCO = 4;
const SEIS = 5;
const ESCALERA = 6;
const FULL = 7;
const POKER = 8;
const GENERALA = 9;
const GENERALADOBLE = 10;

const VALOR_ESCALERA = 20;
const VALOR_FULL = 30;
const VALOR_POKER = 40;
const VALOR_GENERALA = 50;
const VALOR_GENERALADOBLE = 100;
const VALOR_ADICIONAL_SERVIDA = 5;

struct tabla_jugador {
	int id_jugador;
	int categoria[CANT_CATEGORIAS];
};

struct tabla {  	
	struct tabla_jugador tabla_completa[MAX_JUGADORES];
	int cant_jugadores;
};

struct dados {
	int numeros[5];
	int cant_dados;
};

struct turno {
	int id_jugador;
	struct dados jugada;  	 	 
};

struct anotacion {
	int tachar;
	int categoria;
	int valor;
};

program JUEGO_GENERALA {
	version GENERALAVERS {
		 int anotarse()=1;
		 int iniciar()=2;
		 int estado_juego()=3;
		 struct turno quien_juega(int)=4;
		 int fin_turno(int)=5;
		 tabla resultados()=6;
		 struct dados tirar_dados(struct dados)=7;
		 int anotar(struct anotacion)=8;
		 int termine(int)=9;
	} = 1;
} = 0x20900010;
