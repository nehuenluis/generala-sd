all : generala_client generala_server
.PHONY : all

generala_server : generala_server.c generala_svc.o generala_xdr.o
	gcc -ltirpc -I/usr/include/tirpc -o generala_server generala_server.c generala_svc.o generala_xdr.o

generala_svc.o : generala_svc.c
	gcc -ltirpc -I/usr/include/tirpc -c generala_svc.c

generala_client : generala_client.c generala_clnt.o generala_xdr.o
	gcc -ltirpc -I/usr/include/tirpc -o generala_client generala_client.c generala_clnt.o generala_xdr.o

generala_clnt.o : generala_clnt.c
	gcc -ltirpc -I/usr/include/tirpc -c generala_clnt.c

generala_xdr.o : generala_xdr.c
	gcc -ltirpc -I/usr/include/tirpc -c generala_xdr.c	

generala_clnt.c generala_svc.c generala_client.c generala_server.c : generala.x
	rpcgen generala.x

 clean :
	$(RM) *.o generala_client generala_server generala_clnt.c generala_svc.c generala_xdr.c generala.h
